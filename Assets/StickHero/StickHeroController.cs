﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class StickHeroController : MonoBehaviour
{   
    [SerializeField] private StickHeroStick m_Stick;
    [SerializeField] private StickHeroPlayer m_Player;
    [SerializeField] private List <StickHeroPlatform> m_Platforms;
    private StickHeroPlatform add;
    
    private int counter; // счетчик платформы
    
    
    public enum EGameState
    {
        Wait,
        Scaling,
        Rotate,
        Movement,
        Defeat,
    }

    private EGameState currentGameState;
    
    // Start is called before the first frame update
    void Start()
    { 
         
        currentGameState = EGameState.Wait;
        counter = 0;        

        m_Stick.ResetStick(m_Platforms[0].GetStickPosition());
    }
    
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) == false)
        {
            return;
        }
        
        switch (currentGameState)
        {
            // если не осуществлен старт игры
            case EGameState.Wait:
                currentGameState = EGameState.Scaling;
                m_Stick.StartScaling();
                break;
            // если стик увеличивается прирываем и запускаем поворот
            case EGameState.Scaling:
                currentGameState = EGameState.Rotate;
                m_Stick.StopScaling();
                break;
            // ничего не делаем
            case EGameState.Rotate:
                break;
            // ничего не делаем
            case EGameState.Movement:
                break;
            
            case EGameState.Defeat:
                print("Defeat");
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                break;
            
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    public void StopStickScale()
    {
        currentGameState = EGameState.Rotate;
        m_Stick.StartRotate();
    }

    public void StopStickRotate()
    {
        currentGameState = EGameState.Movement;
    }

    public void StartPlayerMovement(float length)
    {
        currentGameState = EGameState.Movement;

        StickHeroPlatform nextPlatform = m_Platforms[counter + 1];
        
        // находим минимальную длину стика для успешного перехода
        float targetLength = nextPlatform.transform.position.x - m_Stick.transform.position.x;

        float platformSize = nextPlatform.GetPlatformSize();
        
        float min = targetLength - platformSize * 0.5f;
        min -= m_Player.transform.localScale.x * 0.9f;
        
        // находим максимальную длину стика для успешного перехода
        float max = targetLength + platformSize * 0.5f;
        
        // при успехе переходим на следующую платформу, иначе падаем
        if (length < min || length > max)
        {
           //будем падать
            float targetPosition = m_Stick.transform.position.x + length + m_Player.transform.localScale.x;            
            m_Player.StartMovement(targetPosition, true);          
        }
        else
        {
            float targetPosition = nextPlatform.transform.position.x;
            m_Player.StartMovement(targetPosition, false);
            float corZ = m_Platforms[m_Platforms.Count-1].transform.position.x + Random.Range(0.5f, 2f); ;
            add = Instantiate(m_Platforms[Random.Range(1,4)], new Vector3(corZ, 0.15f, 0), Quaternion.identity);            
            m_Platforms.Add(add);   
        }
    }

    public void StopPlayerMovement()
    {
        currentGameState = EGameState.Wait;
        counter++;        
        m_Stick.ResetStick(m_Platforms[counter].GetStickPosition());     
    }

    public void ShowScores()
    {
        currentGameState = EGameState.Defeat;
        
        print($"Game over at {counter}");
    }
}
